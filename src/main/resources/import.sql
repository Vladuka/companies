INSERT INTO company (id, name, earnings) VALUES (1, '1_level', 1);
INSERT INTO company (id, name, earnings) VALUES (2, '1_level', 1);
INSERT INTO company (id, name, earnings, parent_id) VALUES (3, '2_level', 10, 1);
INSERT INTO company (id, name, earnings, parent_id) VALUES (4, '2_level', 10, 2);
INSERT INTO company (id, name, earnings, parent_id) VALUES (5, '2_level', 10, 1); 

INSERT INTO company (id, name, earnings, parent_id) VALUES (6, '2_level', 10, 2);
INSERT INTO company (id, name, earnings, parent_id) VALUES (7, '3_level', 100, 3);
INSERT INTO company (id, name, earnings, parent_id) VALUES (8, '3_level', 100, 4);
INSERT INTO company (id, name, earnings, parent_id) VALUES (9, '3_level', 100, 3);
INSERT INTO company (id, name, earnings, parent_id) VALUES (10, '3_level', 100, 4);
INSERT INTO company (id, name, earnings, parent_id) VALUES (11, '4_level', 1000, 7);

INSERT INTO company (id, name, earnings, parent_id) VALUES (12, '4_level', 1000, 8);
INSERT INTO company (id, name, earnings, parent_id) VALUES (13, '4_level', 1000, 7);
INSERT INTO company (id, name, earnings, parent_id) VALUES (14, '4_level', 1000, 8);
INSERT INTO company (id, name, earnings, parent_id) VALUES (15, '5_level', 10000, 11);
INSERT INTO company (id, name, earnings, parent_id) VALUES (16, '5_level', 10000, 12);
INSERT INTO company (id, name, earnings, parent_id) VALUES (17, '2_level', 10000 , 2);
