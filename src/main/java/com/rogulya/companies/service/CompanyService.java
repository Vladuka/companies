package com.rogulya.companies.service;

import java.util.List;

import com.rogulya.companies.domain.Company;

public interface CompanyService {
    List<Company> findAll();

    void create(Company company);

    Company findById(Object id);

    void deleteById(Object id);

    void delete(Company company);

    Company update(Company company);
}
