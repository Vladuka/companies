package com.rogulya.companies.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rogulya.companies.dao.CompanyDao;
import com.rogulya.companies.domain.Company;
import com.rogulya.companies.service.CompanyService;

@Service("companyService")
@Transactional
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyDao dao;

    @Override
    public List<Company> findAll() {
        return dao.findAll();
    }

    @Override
    public void create(Company company) {
        dao.create(company);
    }

    @Override
    public Company findById(Object id) {
        return dao.findById(id);
    }

    @Override
    public void deleteById(Object id) {
        dao.deleteById(id);
    }

    @Override
    public void delete(Company company) {
        dao.delete(company);
    }

    @Override
    public Company update(Company company) {
        return dao.update(company);
    }

}
