package com.rogulya.companies.dao;

import java.util.List;

import com.rogulya.companies.domain.Company;

public interface CompanyDao {

    List<Company> findAll();

    void create(Company company);

    Company findById(Object id);

    void deleteById(Object id);

    void delete(Company company);

    Company update(Company company);

}
