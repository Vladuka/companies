package com.rogulya.companies.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.rogulya.companies.dao.CompanyDao;
import com.rogulya.companies.domain.Company;

@Repository
public class CompanyDaoImpl implements CompanyDao {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Company> findAll() {
        return entityManager.createQuery("select c from Company c").getResultList();
    }

    @Override
    public void create(Company company) {
        entityManager.persist(company);
    }

    @Override
    public Company findById(Object id) {
        return entityManager.find(Company.class, id);
    }

    @Override
    public void deleteById(Object id) {
        Company company = entityManager.find(Company.class, id);
        entityManager.remove(company);
    }

    @Override
    public void delete(Company company) {
        entityManager.remove(company);
    }

    @Override
    public Company update(Company company) {
        return entityManager.merge(company);
    }

}
