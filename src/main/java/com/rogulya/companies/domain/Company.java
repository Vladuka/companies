package com.rogulya.companies.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "earnings")
    private int earnings;

    @ManyToOne
    private Company parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Company> children = new ArrayList<Company>();

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getEarnings() {
        return earnings;
    }

    public Company getParent() {
        return parent;
    }

    public List<Company> getChildren() {
        return children;
    }

    public void addChild(Company child, Company parent) {
        parent.getChildren().add(child);
    }

    public void addChild(Company child) {
        children.add(child);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEarnings(int earnings) {
        this.earnings = earnings;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setChildren(List<Company> children) {
        this.children = children;
    }

    public void setParent(Company parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Comp -- " + name + " his " + parent + "]";
    }

    // @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch =
    // FetchType.EAGER)
    // @JoinColumn(name = "id")
    // @JoinColumn(referencedColumnName = "id")
    // @OnDelete(action = OnDeleteAction.CASCADE)
    // @Cascade(value = { CascadeType.ALL })
    // private ArrayList<Company> child;

}
