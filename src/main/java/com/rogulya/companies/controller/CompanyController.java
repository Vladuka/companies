package com.rogulya.companies.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.einnovates.customtags.tree.TreeNode;
import com.rogulya.companies.domain.Company;
import com.rogulya.companies.service.CompanyService;

@Controller
public class CompanyController {

    @Autowired
    CompanyService compService;

    @RequestMapping(value = { "showAll", "/" })
    public String main(Model model) {
        List<Company> companyList = compService.findAll();
        model.addAttribute("list", companyList);
        return "index";
    }

    @RequestMapping("/newCompany")
    public String newCompany(Model model) {
        model.addAttribute("list", compService.findAll());
        return "newCompany";
    }

    @RequestMapping(value = "/newCompany", method = RequestMethod.POST)
    public String saveNewCompany(Model model, @RequestParam(value = "name") String name,
            @RequestParam(value = "earnings") int earnings,
            @RequestParam(value = "parentId", required = false) Integer parentId) {
        Company company = new Company();
        company.setName(name);
        company.setEarnings(earnings);
        if (parentId != null) {
            Company parent = compService.findById(parentId);
            company.setParent(parent);
        }
        compService.create(company);
        return "redirect:/";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteCompany(HttpServletRequest request) {
        int compId = Integer.parseInt(request.getParameter("id"));
        compService.deleteById(compId);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editCompany(HttpServletRequest request, Model model) {
        int compId = Integer.parseInt(request.getParameter("id"));
        model.addAttribute("company", compService.findById(compId));
        model.addAttribute("list", compService.findAll());
        return "newCompany";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveEditCompany(Model model, @RequestParam(value = "name") String name,
            @RequestParam(value = "id") int id, @RequestParam(value = "earnings") int earnings,
            @RequestParam(value = "parentId", required = false) Integer parentId) {
        Company company = compService.findById(id);
        company.setName(name);
        company.setEarnings(earnings);
        if (parentId != null) {
            Company parent = compService.findById(parentId);
            company.setParent(parent);
        }
        compService.update(company);
        return "redirect:/";
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String viewCompany(HttpServletRequest request, Model model) {
        int compId = Integer.parseInt(request.getParameter("id"));
        model.addAttribute("comp", compService.findById(compId));
        Company comp = compService.findById(compId);
        // TreeNode treeData = new TreeNode(compService.findById(compId));
        // treeData.add(cr(c));
        // model.addAttribute("treeData", cr(c));
        int sum = comp.getEarnings();
        TreeNode treeData = new TreeNode(comp.getName() + " " + comp.getEarnings() + " -- " + cs(comp, sum));

        treeData = (cr(treeData, comp));
        model.addAttribute("treeData", treeData);
        return "view";
    }

    private TreeNode cr(TreeNode treeData, Company comp) {
        if (comp.getChildren() != null) {
            for (Company c : comp.getChildren()) {
                int sum = c.getEarnings();
                TreeNode tr = new TreeNode(c.getName() + " - " + c.getEarnings() + " -- " + cs(c, sum));
                treeData.add(tr);
                if (c.getChildren() != null) {
                    cr(tr, c);
                }
            }
        }
        return treeData;
    }

    private int cs(Company comp, int sum) {
        if (comp.getChildren() != null) {
            for (Company c : comp.getChildren()) {
                sum = c.getEarnings() + cs(c, sum);

            }
        }
        return sum;
    }

    @RequestMapping("/youUrl")
    public @ResponseBody Map<String, Object> getText(HttpServletRequest request) {
        Map<String, Object> modelMap = new HashMap<String, Object>(2);
        modelMap.put("foo", "bar");
        modelMap.put("success", true);
        return modelMap;
    }

}
