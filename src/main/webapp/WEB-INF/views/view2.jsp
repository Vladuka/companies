<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<ul>
    <li>       
        ${comp.name} ${comp.earnings}K  
    </li> 
    <ul>
        <c:forEach var="comp" items="${comp.children}">       
            <c:set var="comp" value="${comp}" scope="request"/>  
            <jsp:include page="view.jsp"/> 
        </c:forEach>  
    </ul>
</ul> 
</body>
</html>