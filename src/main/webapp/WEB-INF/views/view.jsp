<%@ page import="com.rogulya.companies.domain.Company" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://einnovates.com/tree4jsp" prefix="t" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tree</title>
</head>
<body>
      
  <t:tree treeData="${treeData}" 
    imageBase="WEB-INF/images/tree/"
    minusImage="16ma.png" plusImage="16pa.png"
    nodeObject="nodeObject"> 

  </t:tree>





</body>
</html>